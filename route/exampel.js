const express = require("express");
const router = express.Router();
const sql = require("../db");
const fetch = require("node-fetch");

//แสดงข้อมูลทั้งหมดของ collection posts
router.get("/", (req, res) => {
  let version = "SELECT * FROM `keycode_v2hbox`";
  sql.query(version, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      res.json(result);
    }
  });
});

router.post("/example", (req, res) => {
  fetch("http://96.30.124.237:3000/maintenance")
    .then(response => response.json())
    .then(data => {
      console.log("lammmmm :" + JSON.stringify(data));
    })
    .catch(err => {
      console.log(err);
    });
});

router.post("/lamtest", async (req, res) => {
  var url = "http://96.30.124.237:3000/menu_mainpage";
  var sentiments = await fetch(url, {method: "POST", body: {"data": [{"text": "I love you"}, {"text": "I hate you"}]}})
     var xx = await sentiments.json()
     console.log(xx);
});

//Delate mathod
router.delete("/:postIdd", async (req, res) => {
  // รับค่า postIdd จาก ID url
  //const postIdd = req.params.postIdd;
});
module.exports = router;
