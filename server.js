const express = require("express");
const bodyparser = require("body-parser");
const app = express();
require('./db')
app.use(bodyparser.json());
app.use(bodyparser.urlencoded());

const example = require('./route/exampel');
app.use('/example',example);


app.listen(3000,() => {
    console.log("server Is Running......");
});